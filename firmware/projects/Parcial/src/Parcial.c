
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*PROYECTO 3: "SISTEMA DE REGISTRO DE VOLÚMEN DE DEPÓSITO DE AGUA, CON MEDICLIÓN DE TEMPERATURA"*/

/*==================[inclusions]=============================================*/
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "stdio.h"
#include "../inc/Parcial.h"       /* <= own header */


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


int16_t Contador=0;
uint16_t Presion;
int16_t Presion_Acumulada;
int16_t Promedio_Presion;
bool Fin_Conversion=false; //BANDERA para fin de conversiòn
int16_t Presion_Max=0;
int16_t Presion_Min=9999;//elijo inicializarlo en ese valor suponiendo que el Minimo real nunca serà mayor a tal valor.
bool Aux_Tecla_1=false;
bool Aux_Tecla_2=false;
bool Aux_Tecla_3=false;




void Fin_Convertir()
{
	AnalogInputRead(CH1, &Presion);
	Presion=(200-0)/(1024)*Presion+0;//La ecuación de una recta es Y=mX+b, Y sería la presión, m la pendiente(200-0)/1024, b el valor de
	//la función cuando X es cero (que es 0), y X justamente la variable independiente que es lo que nos da el conversor.
	//Uso 1024 porque 2 elevado a la 10 representaría el max voltaje que es 3.3 en 10 bits
	Fin_Conversion=true;
}

void Adquirir()
{
	AnalogStartConvertion();
}


void Lectura_Tecla_1()
{
	Aux_Tecla_1=true;
	Aux_Tecla_2=0;
	Aux_Tecla_3=0;
}

void Lectura_Tecla_2()
{
	Aux_Tecla_2=true;
	Aux_Tecla_1=0;
	Aux_Tecla_3=0;
}

void Lectura_Tecla_3()
{
	Aux_Tecla_3=true;
	Aux_Tecla_1=0;
	Aux_Tecla_2=0;
}

serial_config Activ_serial={SERIAL_PORT_RS485, 115200, NULL};//inicializo la esctructura llamada Activ de la uart

timer_config Activ_timer_Adquirir={TIMER_A, 4, &Adquirir};//uso timer_A, por defecto controla cada 10 ms, le mando 4 porque la Fm=250 Hz.

int main(void){
	analog_input_config inputs={CH1, AINPUTS_SINGLE_READ, &Fin_Convertir};
	UartInit(&Activ_serial);
	TimerInit(&Activ_timer_Adquirir);//inicializo el timer
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, Lectura_Tecla_1);
	SwitchActivInt(SWITCH_2, Lectura_Tecla_2);
	SwitchActivInt(SWITCH_3, Lectura_Tecla_3);
	AnalogInputInit(&inputs);//inicializo la parte analógica (Presión en nuestro caso)
	gpio_t pins[7];
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	ITSE0803DisplayValue(0);
	while(1)
	{
		if(Fin_Conversion==true)
		{
			if(Contador<250)
			{
				Presion_Acumulada+=Presion;
				if(Presion_Max<Presion) //Para el máximo valor de presion dentro de 1 segundo
				{
					Presion_Max=Presion;
				}
				if(Presion_Min>Presion)//Para el mìnimo valor de presion dentro de 1 segundo
				{
					Presion_Min=Presion;
				}
				Contador++;
			}
			else
			{
				Promedio_Presion=(Presion_Acumulada/250);//Hago el promedio de la presion (250 últimos valores)
				//if(Contador==250) //no es necesario porque ya esta el else, osea que ya será 250
				//{
					if(Aux_Tecla_1==true)//si apretó tecla 1 muestro la presiòn màxima en display y uart
					{
						ITSE0803DisplayValue(Presion_Max);
						UartSendString(SERIAL_PORT_PC, UartItoa(Presion_Max, 10));//para mostrar los valores (Presiòn màxima)
						UartSendBuffer(SERIAL_PORT_PC, "mmHg\n\r", strlen("mmHg\n\r"));
						LedOff(LED_1); //Apago led amarillo
						LedOff(LED_3);//Apago led verde
						LedOn(LED_2);//Prendo led rojo
					}
					else
					{
						if(Aux_Tecla_2==true)//si apretó tecla 2 muestro la presiòn mìnima en display y uart y prendo led correspondientes
						{
							ITSE0803DisplayValue(Presion_Min);
							UartSendString(SERIAL_PORT_PC, UartItoa(Presion_Min, 10));//para mostrar los valores (Presion mìnima)
							UartSendBuffer(SERIAL_PORT_PC, "mmHg\n\r", strlen("mmHg\n\r"));
							LedOff(LED_2);//Apago led rojo
							LedOff(LED_3);//Apago led verde
							LedOn(LED_1);//Prendo led amarillo
						}
						else
						{
							if(Aux_Tecla_3==true)//si apretó tecla 3 muestro la presiòn promedio en display y uart y prendo led correspondientes
							{
								ITSE0803DisplayValue(Promedio_Presion);
								UartSendString(SERIAL_PORT_PC, UartItoa(Promedio_Presion, 10));//para mostrar los valores (Promedio de presion)
								UartSendBuffer(SERIAL_PORT_PC, "mmHg\n\r", strlen("mmHg\n\r"));
								LedOff(LED_1);//Apago led amarillo
								LedOff(LED_2);//Apago led rojo
								LedOn(LED_3);//Prendo led verde
							}
						}
					}
				//}

				if(Presion_Max>150)//se deberà prender un led rojo (RGB) si eso pasa
				{
					LedOff(LED_RGB_B);
					LedOff(LED_RGB_G);
					LedOn(LED_RGB_R);
				}
				else
				{
					if(Presion_Max<150 && Presion_Max>50)//se deberà prender un led azul (RGB) si eso pasa
					{
						LedOff(LED_RGB_R);
						LedOff(LED_RGB_G);
						LedOn(LED_RGB_B);
					}
					else
					{
						if(Presion_Max<50)//se deberà prender un led verde (RGB) si eso pasa
						{
							LedOff(LED_RGB_B);
							LedOff(LED_RGB_R);
							LedOn(LED_RGB_G);
						}
					}
				}
				Contador=0;//reseteo los conteos necesarios
				Presion_Acumulada=0;
				Presion_Max=0;
				Presion_Min=9999;
			}
			Fin_Conversion=false;
		}

	}

	return 0;
}



/*==================[end of file]============================================*/

