Diseñar e implementar el firmware de un sistema de adquisición que cumpla con los siguientes requerimientos:

1)Digitalizar señal de un sensor de presión conectado al canal CH1 de la EDU-CIAA  tomando muestras a 250Hz por interrupciones.
 El sensor de presión se comporta de manera lineal, con una salida de 0V para 0 mmHg y 3.3V para 200 mmHg.
2)Obtener el valor máximo, el valor mínimo y el promedio del último segundo de señal. 
3)Seleccionar mediante tres botones de la EDU-CIAA, el parámetro a presentar en un display LCD (máximo, mínimo o promedio). 
El mismo valor se debe enviar a través de la UART a la PC (en ascii). 
4)El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar en el LCD (rojo: máximo, amarillo: mínimo, verde: promedio).
5)Si la presión máxima supera los 150 mmHg se deberá encender el red rojo del led RGB de la EDU-CIAA. Entre 150 y 50 mmHg el led azul y por debajo de 50 mmHg el led verde.


Cosas que tuve en cuenta:
1)
-Como la frecuencia de muestreo es de 250 Hz, por segundo habrà 250 muestras, y el tiempo 1/250=4ms.
-La ecuación de una recta es Y=mX+b, Y sería la presión, m la pendiente(200-0)/1024, b el valor de
la función cuando X es cero (que es 0), y X justamente la variable independiente que es lo que nos da el conversor.
Uso 1024 porque 2 elevado a la 10 representaría el max voltaje que es 3.3 en 10 bits. 

2)
-Para obtener el màximo y el mìnimo en UN segundo, comparo dentro de la cantidad de muestras que estàn en 1 segundo (250 mnuestras)
cual serà el maximo y cual el minimo dentro de bucles de if como se puede ver en el codigo.
-Para obtener el promedio, luego de haber acumulado todos los valores de presion durante 1 segundo (250 muestras), hago el promedio de ese valor 
acumulado dividido las 250 muestras.

3)Para seleccionar mediante botones el paràmetro que voy a mostrar, utilizo interrupciones, mediante banderas que se tooglean de Falso a Verdadero
segùn se apretren las terclas o no. Muestero los valores que correspondan por el display y la PC mediante el Microcontrolador periferico UART.

4)De igual forma que el items anterior, segùn el botòn seleccionado, se prendeà un Led segùn corresponda.

5)Mediante un conjunto de if else, me fijo si los valores de presion MAXIMA se encuentran dentro de los rangos especificados en la consigna y prendo
el Led RGB del color que corresponda. Lo hago una vez que ya se han contado las 250 muestras porque es ahì donde recièn vamos a conocer el màximo dentro 
de un segundo. 

 CONSIDERACIONES:
 
 Colores de led:
 Led_1 ----> Led amarillo
 Led_2 ----> Led rojo
 Led_3 ----> Led Verde
 
 
 ACLARACIONES:
 
 El programa realizara la Conversiòn cada 4 ms.
 La bandera de Fin_de_Conversion me permite realizar los calculos del màximo, mìnimo y promedio una vez que se haya finalizado la conversiòn.
 
 CONEXIONADO
 Podemos encontrarlo en "documentation", ""PlataformasCIAA", "LPC4337", "EDU-CIA-NXP asignaciòn de pines...".
 
