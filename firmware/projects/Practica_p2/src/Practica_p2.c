
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*PROYECTO 3: "SISTEMA DE REGISTRO DE VOLÚMEN DE DEPÓSITO DE AGUA, CON MEDICLIÓN DE TEMPERATURA"*/

/*==================[inclusions]=============================================*/
#include "../inc/Practica_p2.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "stdio.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


int16_t Contador=0;
uint16_t Presion;
int16_t Presion_Acumulada;
int16_t Promedio_Presion;
bool Fin_Conversion=false; //BANDERA
int16_t Presion_Max=0;
bool Bandera_Flanco_Desc=0;
bool Aux_Tecla=false;



void Fin_Convertir()
{
	AnalogInputRead(CH1, &Presion);
	Presion=(125-50)/(1024)*Presion+50;//La ecuación de una recta es Y=mX+b, Y sería la presión, m la pendiente, b el valor de la función cuando X es cero, y X justamente la variable independiente que es lo que nos da el conversor. Usamos 1024 porque 2 elevado a la 10 representaría el max voltaje que es 3.3 en 10 bits
	Fin_Conversion=true;
}

void Adquirir()
{
	TimerStart(TIMER_A);
	AnalogStartConvertion();
}

/*void Lectura()
{
	GPIOActivInt(GPIOGP7, GPIO7, &Adquirir, 0);//Llama a Adquirir si se cumple que el GPIO07 esté en 0.
	if(GPIO7==0)/////// es necesario????????????????????????????????
	{
		Bandera_Flanco_Desc=1;
	}
}*/

void Lectura_Tecla_1()
{
	Aux_Tecla=true;
}

void Lectura_Tecla_2()
{
	Aux_Tecla=false;
}

serial_config Activ_serial={SERIAL_PORT_RS485, 115200, NULL};//inicializo la esctructura llamada Activ de la uart

timer_config Activ_timer_Adquirir={TIMER_A, 10, &AnalogStartConvertion};//uso timer_A, por defecto controla cada 10 ms, le mando 10 porque la Fm=100 Hz.

int main(void){
	analog_input_config inputs={CH1, AINPUTS_SINGLE_READ, &Fin_Convertir};//OJO!! Conectar CH1 cuando use sensor de Temp
	UartInit(&Activ_serial);
	TimerInit(&Activ_timer_Adquirir);//inicializo el timer
	SystemClockInit();
	LedsInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_1, Lectura_Tecla_1);
	SwitchActivInt(SWITCH_2, Lectura_Tecla_2);
	AnalogInputInit(&inputs);//inicializo la parte analógica (Presión en nuestro caso)
	GPIOInit(GPIO7, GPIO_INPUT);
	GPIOActivInt(GPIOGP7, GPIO7, &Adquirir, 0);//Llama a Adquirir si se cumple que el GPIO07 esté en 0.
	gpio_t pins[7];
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	ITSE0803DisplayValue(0);
	while(1)
	{
			if(Aux_Tecla==true)//si apretó tecla 1
			{
				ITSE0803DisplayValue(Presion_Max);
				UartSendString(SERIAL_PORT_PC, UartItoa(Presion_Max, 10));//para mostrar los valores (Promedio de presion)
				UartSendBuffer(SERIAL_PORT_PC, "mmHg\n\r", strlen("mmHg\n\r"));
				LedOff(LED_2);//Apago led rojo
				LedOn(LED_3);//Prendo led verde
			}
			else
			{
				if(Aux_Tecla==false)
				{
				LedOff(LED_2);//apago led verde
				LedOn(LED_3);//prendo led rojo
				TimerStop(TIMER_A);
				}
			}

		if(Fin_Conversion==true)
		{
			if(Contador<100)
			{
				Presion_Acumulada+=Presion;
				if(Presion_Max<Presion) //Para el máximo valor de presion dentro de 1 segundo
				{
					Presion_Max=Presion;
				}
				Contador++;
			}
			else
			{
				Promedio_Presion=(Presion_Acumulada/100);//Hago el promedio de la presion (100 últimos valores)
				Contador=0;
				Presion_Acumulada=0;
				Presion_Max=0;
			}
			Fin_Conversion=false;
		}

	}

	return 0;
}





/*==================[end of file]============================================*/

