Diseñar e implementar el firmware de un sistema de adquisición que cumpla con los siguientes 
requerimientos:

a)Digitalizar señal de un acelerómetro analógico conectado al canal CH1 de la EDU-CIAA  tomando 
muestras a 100Hz utilizando interrupciones. 
b)Se desea implementar un filtrado digital de los datos, el cual responde a la siguiente expresión:
valor_filtrado[n] =  valor_filtrado[n-1] - (lpf_beta * (valor_filtrado[n-1] - valor_crudo[n]))
c)El parámetro lpf_beta caracteriza la respuesta en frecuencia del filtro. Seleccionar mediante 
tres botones de la EDU-CIAA, el parámetro a utilizar, siendo:
lpf_beta  = 0.25 si se presiona la tecla 2
lpf_beta  = 0.55 si se presiona la tecla 3
lpf_beta  = 0.75 si se presiona la tecla 4
d)El sistema deberá informar en el LCD el valor de lpf_beta actual (mostrar sólo la parte decimal)
e)El sistema debe mandar por puerto serie el valor crudo y el valor filtrado en cada muestra 
separados por una coma y un salto de línea final. Ej:
152,150
140,148
…..


Cosas que tuve en cuenta:
a)
-Como la frecuencia de muestreo es de 100 Hz, por segundo habrà 100 muestras, y el tiempo 
1/100=10ms.

b)
-Para obtener el filtrado digital de los datos utilicé la ecuación que aparece en el enunciado, 
una vez que obtuve el valor crudo de aceleración.

c)Para seleccionar mediante 3 botones de la EDU-CIAA el valor que tendrá lpf_beta utilicé 3 
banderas booleanas:
Aux_Tecla2--->Tecla 2--->lpf_beta=0.25
Aux_Tecla3--->Tecla 3--->lpf_beta=0.55
Aux_Tecla4--->Tecla 4--->lpf_beta=0.75

d)Para que el sistema muestre únicamente la parte decimal del valor actual de lpf_beta por LCD 
lo multiplique por 100 porque en las 3 opciones de valores para lpf_beta se trata de un valor con 
dos decimales luego de la coma, y al hacer el producto por 100 quedarán para mostrar sólo esos dos 
decimales en cada caso. (0.25*100=25, 0.55*100=55 y 0.75*100=75). Lo muestro directamente una vez 
que se presiona el botón.

e)Para mostrar en la PC el valor de aceleración crudo y el filtrado separados por una coma y 
un salto de línea final utilicé el siguiente código:

--->UartSendString(SERIAL_PORT_PC, UartItoa(Aceleracion_Cruda, 10));--->para mostrar los valores de
																		aceleracion cruda.
--->UartSendBuffer(SERIAL_PORT_PC, ",", strlen(","));--->para agregar seguidamente la coma.
--->UartSendString(SERIAL_PORT_PC, UartItoa(Aceleracion_Filtrada, 10));--->seguido de la coma muestro
																		la acelearación filtrada.
--->UartSendBuffer(SERIAL_PORT_PC, "\n\r", strlen("\n\r"));--->salto de línea final.

 ACLARACIONES:
 
 El programa realizara la Conversiòn cada 10 ms.
 La bandera de Fin_de_Conversion me permite realizar las acciones que me pide el enunciado (cambio  
 de valores de lpf_beta según el botón presionado, mostrar por LCD el valor actual de lpf_beta,
 mostar en la PC mediante puerto serie el valor de la aceleración cruda y filtrada de la forma en 
 que pide el enunciado) una vez que se haya finalizado la conversión.
 
 CONEXIONADO
 Podemos encontrarlo en "documentation", ""PlataformasCIAA", "LPC4337", "EDU-CIA-NXP asignaciòn de pines...".
 
