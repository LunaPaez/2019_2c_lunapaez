
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*PROYECTO 3: "SISTEMA DE REGISTRO DE VOLÚMEN DE DEPÓSITO DE AGUA, CON MEDICLIÓN DE TEMPERATURA"*/

/*==================[inclusions]=============================================*/
#include "../inc/Parcial_Recuperatorio.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "stdio.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


int16_t Contador=0;
uint16_t Aceleracion_Cruda;
uint16_t Aceleracion_Filtrada; //no lo inicializo porque luego convergerá al valor que debería.
int16_t Aceleracion_Acumulada;
bool Fin_Conversion=false; //BANDERA para fin de conversión
bool Aux_Tecla_2=false;
bool Aux_Tecla_3=false;
bool Aux_Tecla_4=false;
float lpf_beta; //lo definí como float porque tendrá decimales


void Fin_Convertir()
{
	AnalogInputRead(CH1, &Aceleracion_Cruda);
	Aceleracion_Filtrada=Aceleracion_Filtrada-lpf_beta*(Aceleracion_Filtrada-Aceleracion_Cruda);//Ecuación de filtro que me da el enunciado
	Fin_Conversion=true;
}

void Adquirir()
{
	AnalogStartConvertion();
}

void Lectura_Tecla_2()
{
	Aux_Tecla_2=true;
	Aux_Tecla_3=0;
	Aux_Tecla_4=0;
}

void Lectura_Tecla_3()
{
	Aux_Tecla_3=true;
	Aux_Tecla_2=0;
	Aux_Tecla_4=0;
}

void Lectura_Tecla_4()
{
	Aux_Tecla_4=true;
	Aux_Tecla_2=0;
	Aux_Tecla_3=0;
}

serial_config Activ_serial={SERIAL_PORT_RS485, 115200, NULL};//inicializo la esctructura llamada Activ de la uart

timer_config Activ_timer_Adquirir={TIMER_A, 10, &Adquirir};//uso timer_A, por defecto controla cada 10 ms, le mando 10 porque la Fm=100 Hz.

int main(void){
	analog_input_config inputs={CH1, AINPUTS_SINGLE_READ, &Fin_Convertir};
	UartInit(&Activ_serial);
	TimerInit(&Activ_timer_Adquirir);//inicializo el timer
	SystemClockInit();
	SwitchesInit();
	SwitchActivInt(SWITCH_2, Lectura_Tecla_2);
	SwitchActivInt(SWITCH_3, Lectura_Tecla_3);
	SwitchActivInt(SWITCH_4, Lectura_Tecla_4);
	AnalogInputInit(&inputs);//inicializo la parte analógica (Presión en nuestro caso)
	gpio_t pins[7];
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	ITSE0803DisplayValue(0);
	while(1)
	{
		if(Fin_Conversion==true)
		{
			if(Aux_Tecla_2==true)//si apretó tecla 2 lpf_beta=0.25
			{
				lpf_beta=0.25;
				ITSE0803DisplayValue(lpf_beta*100);//muestro por LCD el valor actual de
				//lpf_beta sólo la parte decimal, por eso lo multiplico por 100 para que
				//me quede sólo esa parte, por ejemplo en este caso 25
			}
			else
			{
				if(Aux_Tecla_3==true)//si apretó tecla 2 lpf_beta=0.55
				{
					lpf_beta=0.55;
					ITSE0803DisplayValue(lpf_beta*100);//muestro por LCD el valor actual de
					//lpf_beta sólo la parte decimal, por eso lo multiplico por 100 para que
					//me quede sólo esa parte, por ejemplo en este caso 55
				}
				else
				{
					if(Aux_Tecla_4==true)//si apretó tecla 3 lpf_beta=0.75
					{
						lpf_beta=0.75;
						ITSE0803DisplayValue(lpf_beta*100);//muestro por LCD el valor actual de
						//lpf_beta sólo la parte decimal, por eso lo multiplico por 100 para que
						//me quede sólo esa parte, por ejemplo en este caso 75
					}
				}
			}
			UartSendString(SERIAL_PORT_PC, UartItoa(Aceleracion_Cruda, 10));//para mostrar los valores
			//en PC de Aceleracion cruda y filtrada separadas por una coma y luego un salto de línea final
			UartSendBuffer(SERIAL_PORT_PC, ",", strlen(","));//para agregar la coma
			UartSendString(SERIAL_PORT_PC, UartItoa(Aceleracion_Filtrada, 10));//seguido de la coma
			UartSendBuffer(SERIAL_PORT_PC, "\n\r", strlen("\n\r"));//salto de línea final
			Fin_Conversion=false;
		}
	}

	return 0;
}



/*==================[end of file]============================================*/

