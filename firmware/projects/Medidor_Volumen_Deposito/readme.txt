Sistema de registro de volumen de un depósito de agua

El sistema deberá contar con las siguientes funcionalidades:
Adquirir volumen  y temperatura cada 250 ms usando timer e interrupciones.
Mostrar el promedio de los últimos 4 valores de ambas cada 1 seg.
Informar el rango de volumen del agua con el led RGB de la siguiente manera:
Naranja por debajo de 30 % del total
Rojo entre 30 y 70 % del total
Verde mayor de 70.

Informar el valor promedio de volumen y temperatura a través de alguno de los puertos series disponibles en la EDU-CIAA.

