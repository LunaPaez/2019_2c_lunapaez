
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*PROYECTO 3: "SISTEMA DE REGISTRO DE VOLÚMEN DE DEPÓSITO DE AGUA, CON MEDICLIÓN DE TEMPERATURA"*/

/*==================[inclusions]=============================================*/
#include "../inc/Medidor_Volumen_Deposito.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "led.h"
#include "analog_io.h"
#include "ili9341.h"
#include "spi.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "stdio.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


int16_t Distance;
int16_t Volumen;
int16_t Volumen_Acumulado;
int16_t Promedio_Vol;
#define PI 3.14159
#define Radio 3.5
#define Area PI*Radio*Radio
#define Distance_max 12.6
int16_t Contador=0;
#define Vol_Max Area*Distance_max
//uint16_t foreground=10;//color de letras
//uint16_t background=25;//color de fondo
char Data_Vol[20]="Volumen";
char Data_Temp[20]="Temperatura";
uint16_t Temp;
int16_t Temp_Acumulada;
int16_t Promedio_Temp;
bool Fin_Conversion=false; //BANDERA
int16_t Volumen_Max=0;
char Data_cm[20]="cc";
char Data_grad[20]="Grados";
serial_config Activ_serial={SERIAL_PORT_RS485, 115200, NULL};//inicializo la esctructura llamada Activ de la uart

void Fin_Convertir()
{
	AnalogInputRead(CH1, &Temp);
	Fin_Conversion=true;
}


void Adquirir()//Adquiere la medición del promedio de volumen cada 1 segundo
{
	AnalogStartConvertion();
}


timer_config Activ_timer_Adquirir={TIMER_A, 250, &Adquirir};//uso timer_A, y recive 10 porque ya el A por defecto controla cada 10 ms, entonces lo multiplico por 10 para obtener los 1000 ms (1s)

int main(void){
	analog_input_config inputs={CH1, AINPUTS_SINGLE_READ, &Fin_Convertir};//OJO!! Conectar CH1 cuando use sensor de Temp
	UartInit(&Activ_serial);
	TimerInit(&Activ_timer_Adquirir);//inicializo el timer
	TimerStart(TIMER_A);
	SystemClockInit();
	LedsInit();
	ILI9341Init(SPI_1, GPIO5, GPIO3, GPIO1);//GPIO5: gpio_cs, GPIO3: gpio_dc, GPIO1: gpio_rst.
	HcSr04Init(T_FIL2, T_FIL3);//llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
	AnalogInputInit(&inputs);//inicializo la parte analógica (Temperatura en nuestro caso)
	LedOff(LED_RGB_G);
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_R);
	while(1)
	{
		if(Fin_Conversion==true) //????????? DURA MÁS DE 25O MS ?? ENTONCES NO SERÍA EN 1 SEGUNDO EL CONTADOR CUANDO LLEGA A 4????
		{
		Distance=HcSr04ReadDistanceCentimeters();//llamo la función que me devuelve la distancia en cm para medir distancia del senor (colocado en borde superior de depósito) hasta interfaz agua
		if(Distance>Distance_max)
		{
			Distance=Distance_max;
		}
		Volumen=Area*(Distance_max-Distance);
		if(Contador<4)
			{
				Volumen_Acumulado+=Volumen;
				Temp_Acumulada+=Temp;
				if(Volumen_Max<Volumen) //Me muestra el máximo valor de volúmen dentro de 1 segundo
					{
					Volumen_Max=Volumen;
					}
				Contador++;
				}
					else
					{
						Promedio_Vol=(Volumen_Acumulado/4);//Hago promedio de los últimos 4 valores tomados de volumen
						Promedio_Temp=(Temp_Acumulada/4);//Hago el promedio de la temperatura(4 últimos valores)
						ILI9341DrawString(20, 50, Data_Vol, &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display letras
						ILI9341DrawInt(20, 100, Promedio_Vol, 5,  &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display dígitos(promedio de volúmen )
						ILI9341DrawString(20, 150, Data_Temp, &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display letras
						ILI9341DrawInt(20, 200, Promedio_Temp, 5,  &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display dígitos(promedio de volúmen )
						ILI9341DrawString(120, 100, Data_cm, &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display letras
						ILI9341DrawString(120, 200, Data_grad, &font_16x26, ILI9341_BLACK, ILI9341_PINK);//para escribir en el display letras
						//UartSendString(SERIAL_PORT_RS485, UartItoa(Area, 10));
						//UartSendBuffer(SERIAL_PORT_RS485, "Cm_Cuadrado\n\r", strlen("Cm_Cuadrado\n\r"));
						//UartSendString(SERIAL_PORT_RS485, UartItoa(Distance, 10));
						//UartSendBuffer(SERIAL_PORT_RS485, "Cm\n\r", strlen("Cm\n\r"));
						UartSendString(SERIAL_PORT_RS485, UartItoa(Promedio_Vol, 10));//para mostrar en la compu los valores (Promedio de vol)
						UartSendBuffer(SERIAL_PORT_RS485, "Cm_Cubico\n\r", strlen("Cm_Cubico\n\r"));
						UartSendString(SERIAL_PORT_RS485, UartItoa(Promedio_Temp, 10));
						UartSendBuffer(SERIAL_PORT_RS485, "Grados_Centigrados\n\r", strlen("Grados_Centigrados\n\r"));
						UartSendString(SERIAL_PORT_RS485, UartItoa(Volumen_Max, 10)); //Para mostrar el volúmen max
					    UartSendBuffer(SERIAL_PORT_RS485, "Cm_Cubico_max\n\r", strlen("Cm_Cubico_max\n\r"));
						Contador=0;
						Volumen_Acumulado=0;
						Temp_Acumulada=0;
						Volumen_Max=0;
					}
				if(Promedio_Vol<(0.3*Vol_Max))
				{
					LedOff(LED_2);
					LedOff(LED_3);
					LedOn(LED_1);
				}
				else
				{
				if((Promedio_Vol<=(0.7*Vol_Max)) && (Promedio_Vol>(0.3*Vol_Max)))
				{
					LedOff(LED_1);
					LedOff(LED_3);
					LedOn(LED_2);
				}
				else
				{
					LedOff(LED_2);
					LedOff(LED_1);
					LedOn(LED_3);
				}
				}
				Fin_Conversion=false;
		}

	}
	return 0;
}





/*==================[end of file]============================================*/

