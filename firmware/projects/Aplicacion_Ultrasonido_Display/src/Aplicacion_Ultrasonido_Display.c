
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*==================[inclusions]=============================================*/
#include "../inc/Aplicacion_Ultrasonido_Display.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "hc_sr4.h"
#include "switch.h"
#include "delay.h"
#include "led.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/
bool Key_aux_Tec1=false;
bool Key_aux_Tec2=false;
bool Key_aux_Tec3=false;
bool Key_aux_Tec4=false;
bool Distance_bool=false;
int16_t Distance;

void Activate_stop_measurement()//Interrupción que activa y desactiva la medición
{
	Key_aux_Tec1^=true;
}

void Hold()//Interrupción para mantener el valor en display
{
	Key_aux_Tec2^=true;
}

void measurement_cm()//Interrupción que mide en cm
{
	Key_aux_Tec3=false;
	LedOn(LED_RGB_R);
}

void measurement_inches()//Interrupción que mide en pulgadas
{

	Key_aux_Tec3=true;
	LedOn(LED_RGB_G);
}


int main(void){
	SystemClockInit();
	SwitchActivInt(SWITCH_1, Activate_stop_measurement);
	SwitchActivInt(SWITCH_2, Hold);
	SwitchActivInt(SWITCH_3, measurement_cm);
	SwitchActivInt(SWITCH_4, measurement_inches);
	gpio_t pins[7];
	uint8_t LED;
	LED=LedsInit();
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	bool Display=ITSE0803Init(pins);
	SwitchesInit();//inicializa el drivers tecla
	HcSr04Init(T_FIL2, T_FIL3);//llamo la funcion para que conexione ECHO con T_FIL, y TRIGGER con T_FIL3
	ITSE0803DisplayValue(0);
	while (1){
		DelayMs(200);
		 if(Key_aux_Tec1==true)//Está apretada una tecla?
		 {
			 if(Key_aux_Tec3==false)
			 {
				 Distance=HcSr04ReadDistanceCentimeters();//llamo la función que me devuelve la distancia en cm

			 }
			 else
			 {
				 Distance=HcSr04ReadDistanceInches();//llamo la función que me devuelve la distancia en pulgadas

			 }
			 if(Key_aux_Tec2==false)//Como esta toogleado, cuando se apreta la tecla 2 cambia a falso y muestra el valor que tenia anteriormente nomas
			 {
				 ITSE0803DisplayValue(Distance);
			 }

		}
		 else
		 {
			 ITSE0803DisplayValue(0);
		 }

	}

	return 0;
}








/*if(Distance==ITSE0803DisplayValue(distance_cm)){
	ITSE0803ReadValue();//devuelve el valor que lee
	DelayMs(50);//para que espere 50 ms cuando se apreta una tecla
	Varaiable_aux=//para que cambie verdadero o falso
}
	else {
		ITSE0803DisplayValue(distance_inches);
		ITSE0803ReadValue();
		DelayMs(50);//para que espere 50 ms cuando se apreta una tecla
	}
	return 0;
}*/


/*Proyecto: Medidor de distancia por ultrasonido
Mostrar distancia medida por LCD.
Usar TEC1 para activar y detener la medición.
Usar TEC2 para mantener el resultado (“HOLD”).
Usar TEC3 para configurar la medición en cm.
Usar TEC4 para configurar la medición en plg.

* Usar INTERRUPCIONES para el manejo de las teclas.*/


    

/*==================[end of file]============================================*/

