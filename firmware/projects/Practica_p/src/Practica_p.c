
/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * JMReta - jmreta@ingenieria.uner.edu.ar
 *
 *
 *
 * Revisión:
 * 07-02-18: Versión inicial
 * 01-04-19: V1.1 SM
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*PROYECTO 3: "SISTEMA DE REGISTRO DE VOLÚMEN DE DEPÓSITO DE AGUA, CON MEDICLIÓN DE TEMPERATURA"*/

/*==================[inclusions]=============================================*/
#include "../inc/Practica_p.h"       /* <= own header */
#include "systemclock.h"
#include "gpio.h"
#include "DisplayITS_E0803.h"
#include "switch.h"
#include "led.h"
#include "analog_io.h"
#include "timer.h"
#include "uart.h"
#include "string.h"
#include "stdio.h"


/*==================[macros and definitions]=================================*/

/*==================[internal data definition]===============================*/

/*==================[internal functions declaration]=========================*/

/*==================[external data definition]===============================*/

/*==================[external functions definition]==========================*/


int16_t Distance;
int16_t Contador=0;
uint16_t Temp;
int16_t Temp_Acumulada;
int16_t Promedio_Temp;
bool Fin_Conversion=false; //BANDERA
int16_t Temp_Max=0;
int16_t Temp_Min=1000; //Lo inicializo en 1000 suponiendo que nunca llegará a esa temperatura, y siempre la máxima del sensor será menor a 1000.
uint8_t Leyo_o_No=0;
uint8_t Tecla;// En ASCII (S:83, I:73, P:80)

void Fin_Convertir()
{
	AnalogInputRead(CH1, &Temp);
	Fin_Conversion=true;
}


void Adquirir()//Adquiere la medición del promedio de volumen cada 1 segundo
{
	AnalogStartConvertion();
}

void Lectura()
{
	Leyo_o_No=UartReadByte(SERIAL_PORT_RS485, &Tecla);//devuelve 0 o 1 si no lee, o si lee respectivamente, en Tecla devuele en ASCII la tecla que leyó
}


serial_config Activ_serial={SERIAL_PORT_RS485, 115200, Lectura};//inicializo la esctructura llamada Activ de la uart

timer_config Activ_timer_Adquirir={TIMER_A, 50, &Adquirir};//uso timer_A, por defecto controla cada 10 ms, le mando 50 porque la Fm=20 Hz.

int main(void){
	analog_input_config inputs={CH1, AINPUTS_SINGLE_READ, &Fin_Convertir};//OJO!! Conectar CH1 cuando use sensor de Temp
	UartInit(&Activ_serial);
	TimerInit(&Activ_timer_Adquirir);//inicializo el timer
	TimerStart(TIMER_A);
	SystemClockInit();
	LedsInit();
	AnalogInputInit(&inputs);//inicializo la parte analógica (Temperatura en nuestro caso)
	LedOff(LED_RGB_G);
	LedOff(LED_RGB_B);
	LedOff(LED_RGB_R);
	gpio_t pins[7];
	pins[0]=GPIO5;
	pins[1]=GPIO3;
	pins[2]=GPIO1;
	pins[3]=LCD1;
	pins[4]=LCD2;
	pins[5]=LCD3;
	pins[6]=LCD4;
	ITSE0803Init(pins);
	ITSE0803DisplayValue(0);
	while(1)
	{
		if(Leyo_o_No==true)
		{
			if(Tecla==80)
			{
				ITSE0803DisplayValue(Promedio_Temp);
				LedOff(LED_2);
				LedOff(LED_1);
				LedOn(LED_3);//Prendo led verde
			}
			else
			{
				if(Tecla==83)
				{
					ITSE0803DisplayValue(Temp_Max);
					LedOff(LED_1);
					LedOff(LED_3);
					LedOn(LED_2);//Prendo led rojo
				}
				else
				{
					if(Tecla==73)
					{
						ITSE0803DisplayValue(Temp_Min);
						LedOff(LED_2);
						LedOff(LED_3);
						LedOn(LED_1);//Prendo led amarillo
					}
				}

			}
			Leyo_o_No=0;
		}
		if(Fin_Conversion==true)
		{
			if(Contador<20)
			{
				Temp_Acumulada+=Temp;
				if(Temp_Max<Temp) //Para el máximo valor de volúmen dentro de 1 segundo
				{
					Temp_Max=Temp;
				}
				if(Temp_Min>Temp)
				{
					Temp_Min=Temp;
				}
				Contador++;
			}
			else
			{
				Promedio_Temp=(Temp_Acumulada/20);//Hago el promedio de la temperatura(20 últimos valores)


				Contador=0;
				Temp_Acumulada=0;
				Temp_Max=0;
				Temp_Min=1000;
			}
			Fin_Conversion=false;
		}

	}

	return 0;
}





/*==================[end of file]============================================*/

