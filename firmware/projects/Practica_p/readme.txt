Diseñar e implementar el firmware de un sistema de adquisición que cumpla con los siguientes requerimientos:

a.	Digitalizar una señal de un sensor de temperatura conectado al canal CH1 de la EDU-CIAA a una frecuencia de muestreo de 20Hz. Modificar el driver que considere necesario para la temporización y emplear interrupciones: Timer y ADC. 
b.	Calcular el valor máximo, el valor mínimo y el promedio del último segundo de señal. 
c.	Seleccionar mediante tres teclas de la PC, a través de la comunicación serie, el parámetro a presentar en un display LCD (máximo, mínimo o promedio). El mismo valor se debe enviar a través de la UART a la PC (en ascii). Emplear la interrupción de recepción de datos de la UART.
d.	El sistema deberá informar mediante la activación de los led el valor seleccionado a presentar en el LCD (rojo: máximo, amarillo: mínimo, verde: promedio).
 
