########################################################################
#Cátedra: Electrónica Programable
#FIUNER
#2018
#Autor/es:
#JMreta - jmreta@ingenieria.uner.edu.ar
#
#Revisión:
########################################################################

# Ruta del Proyecto
####Ejemplo 1: Hola Mundo
##Por el primero, se debe usar Makefile1
##PROYECTO_ACTIVO = 1_hola_mundo
##NOMBRE_EJECUTABLE = hola_mundo.exe

####Ejemplo 2: Control Mundo
#PROYECTO_ACTIVO = 2_control_mundo
#NOMBRE_EJECUTABLE = control_mundo.exe

####Ejemplo 3: Promedio
#PROYECTO_ACTIVO = 3_promedio_1
#NOMBRE_EJECUTABLE = promedio.exe

#PROYECTO_ACTIVO = 5_menu
#NOMBRE_EJECUTABLE = menu.exe

#PROYECTO_ACTIVO = Ej_c_d_e
#NOMBRE_EJECUTABLE = c_d_e.exe

####Guía 1: Ejercicio 7
#PROYECTO_ACTIVO = guía1_7
#NOMBRE_EJECUTABLE = guía1_7.exe

####Guía 1: Ejercicio 9
#PROYECTO_ACTIVO = guía1_9
#NOMBRE_EJECUTABLE = guía1_9.exe

####Guía 1: Ejercicio 12
#PROYECTO_ACTIVO = guía1_12
#NOMBRE_EJECUTABLE = guía1_12.exe

####Guía 1: Ejercicio 14
#PROYECTO_ACTIVO = guía1_14
#NOMBRE_EJECUTABLE = guía1_14.exe

####Guía 1: Ejercicio 16
#PROYECTO_ACTIVO = guía1_16
#NOMBRE_EJECUTABLE = guía1_16.exe

####Guía 1: Ejercicio Integrador A
#PROYECTO_ACTIVO = guía1_IA
#NOMBRE_EJECUTABLE = guía1_IA.exe

####Guía 1: Ejercicio Integrador C
#PROYECTO_ACTIVO = guía1_IC
#NOMBRE_EJECUTABLE = guía1_IC.exe

####Guía 1: Ejercicio Integrador B
PROYECTO_ACTIVO = guía1_IB
NOMBRE_EJECUTABLE = guía1_IB.exe


####Guía 1: Ejercicio Integrador D
#PROYECTO_ACTIVO = guía1_ID
#NOMBRE_EJECUTABLE = guía1_ID.exe

