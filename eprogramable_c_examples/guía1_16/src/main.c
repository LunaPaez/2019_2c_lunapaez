/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
//a. Declare una variable sin signo de 32 bits y cargue el valor 0x01020304.
//Declare cuatro variables sin signo de 8 bits y, utilizando máscaras, rotaciones y
//truncamiento, cargue cada uno de los bytes de la variable de 32 bits. b.
//Realice el mismo ejercicio, utilizando la definición de una “union”.
/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
#include <stdio.h>
#include <stdint.h>
#include <string.h>



int main(void)
{uint32_t A;
A=0x01020304;
uint8_t B, C, D, E;
E=A;//le asigno a E los últimos 8 bits, (siempre a la variable de menor tamaño se le asgnan los ultimos bits, los menos significativos)
printf("%d ES E\r\n",E);
D=(A>>8); //lo corro 8 bit para la derecha para copiar los que le siguen a los que copie en E
printf("%d ES D\r\n",D);
C=(A>>16);//lo corro 16 bit a la derecha para copiar los que le siguen a los que copie en D
printf("%d ES C\r\n",C);
B=(A>>24);//lo corro 24 bit a la derecha para copiar los que le siguen a los que copie en C
printf("%d ES B\r\n",B);
union test{
	uint32_t  todos_los_bytes;
	struct {
		uint8_t byte1;
		uint8_t byte2;
		uint8_t byte3;
		uint8_t byte4;
	} cada_byte;
}U;//cree una union que contiene un entero sin signo de 32 bits y una estructura con 4 enteros sin signo. Cada_byte es una variable del tipo struct y U es una variable del tipo test (union)
U.todos_los_bytes=A;
uint8_t Eu, Du, Cu, Bu;
Eu=U.cada_byte.byte1;//para acceder a cada elemento de la estructura dentro de la union
Du=U.cada_byte.byte2;
Cu=U.cada_byte.byte3;
Bu=U.cada_byte.byte4;
printf("%d ES Eu\r\n",Eu);
printf("%d ES Du\r\n",Du);
printf("%d ES Cu\r\n",Cu);
printf("%d ES Bu\r\n",Bu);
return 0;
}

/*==================[end of file]============================================*/

