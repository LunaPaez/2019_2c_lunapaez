/*
 * Cátedra: Electrónica Programable
 * FIUNER - 2018
 * Autor/es:
 * 
 *
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */
//Declare un puntero a un entero con signo de 16 bits y cargue inicialmente el valor -1.
//Luego, mediante máscaras, coloque un 0 en el bit 4.
/*==================[inclusions]=============================================*/
#include "main.h"

/*==================[macros and definitions]=================================*/


/*==================[internal functions declaration]=========================*/
#include <stdio.h>
#include <stdint.h>

int main(void)
{int16_t A, *punt;// tengo un entero A de 16 bit y un puntero llamado punt que apunta a eneteros de 16 bit
punt=&A;//Asignación de la dirección memoria de A
*punt=-1;//le asigno a A el valor -1 a través del puntero
printf("%d ES A\r\n",*punt);//para mostrar el valor de A
*punt=(*punt)&~(1<<4);//a lo que apunta punt (A) le asigno lo mismo pero con el bit 4 en 0
printf("%d ES EL NUEVO VALOR DE A\r\n",*punt);//para mostrar el nuevo valor de A
	return 0;
} //¿ME DEBERÍA DAR EL NUEVO VALOR DE A=-1 PORQUE EL BIT 4 YA ESTABA EN CERO Y LO VOLVÍ A PONER EN CERO?
//NO ME DA, ME DA a=-17 COMO QUE AHORA EL BIT 4 ESTÁ EN 1 EN VEZ DE CERO
/*==================[end of file]============================================*/

